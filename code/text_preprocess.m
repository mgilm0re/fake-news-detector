% the data we received wasn't always consistent
% for instance sometimes contractions were incorrect
% and appeared as "don t" instead of "don't" so here
% we are rectifying this problem

function data_out = text_preprocess(data_in)
	
	% makes all letters lower case
	text = cellstr(lower(data_in));
	
	% gets number of articles
	[ROWS,~] = size(text);
	
	% characters to not replace
	keep_chars = [32  39 48:57 97:255];

	% turns all no alpha-numeric charater except ' and a space with
	% a space and keeps U.S. as U.S.
	for i = 1:ROWS
		foo = text{i};
		foo(not(ismember(text{i},keep_chars))) = ' ';
		foo = replace(foo," u s ", " United-States ");
		foo = replace(foo," united states ", " United-States ");
		text{i} = foo;
	end
	
	% replaces n spaces with one space
	text = regexprep(text, " +", " ");

	% common contraction suffixes
	suf = ["s" "t" "ve"];
	
	% pattern for contraction without '
	old = " " + suf' + " ";
	
	% pattern for contraction with '
	new = "'" + suf' + " ";
	
	% inserts ' for contractions missing it
	text = replace(text, old, new);
	
	% returns processed strings
	data_out = string(text);
end