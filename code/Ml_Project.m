%% data preprocessing

% Read in the and process the true CSV
true_filename = 'True.csv';
true_data = readtable(true_filename, 'TextType', 'string');
true_data.text = text_preprocess(true_data.text);

% Read in the and process the false CSV
false_filename = 'Fake.csv';
false_data = readtable(false_filename, 'TextType', 'string');
false_data.text = text_preprocess(false_data.text);

% creates veracity for true data
[true_rows, ~] = size(true_data);
true_veracity(1:true_rows,1) = "T";

% creates veracity for flase data
[false_rows, ~] = size(false_data);
false_veracity(1:false_rows,1) = "F";

% Labeling the data from the true and fake data sets accordinly within their
% respective tables
 true_data = [ true_data(:,:) table( true_veracity, 'VariableNames', {'Veracity'})];
false_data = [false_data(:,:) table(false_veracity, 'VariableNames', {'Veracity'})];

% combines tables
data = [true_data ; false_data];

%% data partitioning

% partitions the data for training and testing
cvp = cvpartition(data.Veracity,'Holdout',0.2);
dataTrain = data(training(cvp),:);
dataValidation = data(test(cvp),:);

textDataTrain = dataTrain.text;
textDataValidation = dataValidation.text;
YTrain = categorical(dataTrain.Veracity);
YValidation = categorical(dataValidation.Veracity);


tokensTrain = tokenizedDocument(textDataTrain);
tokensValidation = tokenizedDocument(textDataValidation);

%In order for our machine learning model to be able to understand we use
%a word encoding function to convert each of the tokens to a vector that
%the machine will be able to understand numerically.
enc = wordEncoding(tokensTrain);

%% data visualization

% show word cloud of training data to get an idea of what we can
% understand about our data at a high level
figure;
wordcloud(textDataTrain);
title("Training Data")

textLengths = doclength(tokensTrain);
figure;
histogram(textLengths)
title("Document Lengths")
xlabel("Length")
ylabel("Number of Documents")

%%

sequenceLength = 1000;
XTrain = doc2sequence(enc,tokensTrain,'Length',sequenceLength);
XTrain(1:5)

XValidation = doc2sequence(enc,tokensValidation,'Length',sequenceLength);

inputSize = 1;
embeddingDimension = 50;
numHiddenUnits = 80;

numWords = enc.NumWords;
numClasses = numel(categories(YTrain));

layers = [ ...
    sequenceInputLayer(inputSize)
    wordEmbeddingLayer(embeddingDimension,numWords)
    lstmLayer(numHiddenUnits,'OutputMode','last')
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer]

options = trainingOptions('adam', ...
    'MiniBatchSize',16, ...
    'GradientThreshold',2, ...
    'Shuffle','every-epoch', ...
    'ValidationData',{XValidation,YValidation}, ...
    'Plots','training-progress', ...
    'Verbose',false)
%% train network

net = trainNetwork(XTrain,YTrain,layers,options);







